# Choose a Job to Run



When you run a pipeline manually, you're given a list of "jobs" to run.

To get a similar functionality with "Scheduled Pipelines", you can manually
defined a CI Variable with the job name.
